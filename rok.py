#!/usr/bin/python
# pip install pure-python-adb pillow pytesseract
# pacman -Syu tesseract sox android-tools

from PIL import Image, ImageOps
from ppadb.client import Client
import argparse
import io
import json
import os
import pickle
import pytesseract
import readline
import sys
import time
import traceback


class GameError(Exception):
    def __init__(self, quit=False):
        setattr(self, 'quit', quit)


class GatherGen:
    __rss = ["food", "wood", "stone", "gold"]
    # __rss = ["wood", "stone"]
    __barb = [-1, 0, 1]

    def __init__(self):
        self.__now = self.__rss[0]
        self.__nowbarb = self.__barb[0]

    def next(self):
        index = (self.__rss.index(self.__now)+1) % len(self.__rss)
        self.__now = self.__rss[index]
        return self.__now

    def nextbarb(self):
        index = (self.__barb.index(self.__nowbarb)+1) % len(self.__barb)
        self.__nowbarb = self.__barb[index]
        return self.__nowbarb


class Stats:
    helps = 0
    marches = "TBD"
    maintenance = 0
    ap = -1
    captcha_t = []

    @classmethod
    def summary(cls):
        tech = ''.join(map(str, args.tech)) if args.tech else 'NaN'
        return "Helps: {:<5d} Marches: {:<5s} Maintenance: {:<5d}" \
            "Last known AP: {:<4d} [{}]".format(
                cls.helps, cls.marches, cls.maintenance, cls.ap, tech)


class Device:
    serial = None
    height = None
    width = None
    dpi = None
    device = None

    def __init__(self, device):
        self.device = device
        self.serial = device.serial
        wm = device.shell('wm size')[15:-1].split('x')
        self.height, self.width = map(int, wm)
        self.dpi = int(device.shell('wm density')[18:-1])

    def __str__(self):
        return "Serial: {}, Dimension: {}x{}, Density: {}".format(
            self.serial, self.width, self.height, self.dpi)

    def shell(self, cmd):
        return self.device.shell(cmd) if cmd else None

    def event(self, keycode, sleep=1.2):
        self.device.shell("input keyevent KEYCODE_{}".format(keycode.upper()))
        time.sleep(sleep)

    def tap(self, x, y, sleep=1.2):
        self.device.shell("input tap {:.0f} {:.0f}".format(
            self.width*x/100, self.height*y/100))
        time.sleep(sleep)

    def tapa(self, x, y, sleep=1.2):
        self.device.shell("input tap {:.0f} {:.0f}".format(x, y))
        time.sleep(sleep)

    def longtap(self, x, y, duration=500, sleep=1.2):
        self.device.shell(
            "input swipe {0:.0f} {1:.0f} {0:.0f} {1:.0f} {dur}".format(
                self.width*x/100, self.height*y/100, dur=duration))
        time.sleep(sleep)

    def longtapa(self, x, y, duration=3000, sleep=1.2):
        self.device.shell(
            "input swipe {0:.0f} {1:.0f} {0:.0f} {1:.0f} {dur}".format(
                x, y, dur=duration))
        time.sleep(sleep)

    def run(self, *args, **kargs):
        sleep = kargs.get('sleep', 1.2)
        for x, y in zip(args[0::2], args[1::2]):
            confirm(self)
            if x == 0 and y == 0:
                self.tap(*tapseq["popupm"], sleep=sleep)
            else:
                self.tap(x, y, sleep=sleep)

    def screencap(self):
        return self.device.screencap()


def argparser():
    parser = argparse.ArgumentParser(
        description="Keep Rise of Kingdoms running while afk")
    parser.add_argument(
        "--safe", dest="safe", action="store_true", help="Avoid city taps")
    parser.add_argument(
        "-l", dest="log", action="count", default=0, help="Log tess images")
    parser.add_argument(
        "-s", dest="silent", action='count', default=0, help="Try to be quiet")
    parser.add_argument(
        "-c", dest="chain", action="store_true", help="Chain barb hunt")
    parser.add_argument(
        "-d", dest="device", type=int, default=0, help="Device # in adb")
    parser.add_argument(
        "-H", dest="host", type=str, default="127.0.0.1",
        help="ADB server host")
    parser.add_argument(
        "-p", dest="port", type=int, default=5037, help="ADB server port")
    parser.add_argument(
        "-m", dest="marches", type=int, default=4, help="Maximum marches")
    parser.add_argument(
        "-a", dest="ap", type=int, default=840, help="Retain AP level")
    parser.add_argument(
        "-t", dest="tech", type=tech_coords, help="Donate Research")
    parser.add_argument(
        "-T", dest="tier", type=int, help="Troops training")
    return parser


def tech_coords(string):
    result = list(map(int, list(string)))
    correct = len(result) == 3 and all(result)
    correct &= result[0] < 4 and result[1] < 5 and result[2] < 5
    if not correct:
        raise ValueError('{} is not a correct coord'.format(string))
    return result


def getAdbDevice(client):
    index = args.device
    devices = []
    try:
        devices = client.devices()
    except RuntimeError:
        # traceback.print_exc(1)
        print("ADB Server not found at {}:{}".format(args.host, args.port))
        sys.exit(-1)

    if len(devices) > index:
        return devices[index]
    else:
        print("Device# {} not found".format(index))
        sys.exit(-1)


def beep(times=1):
    for i in range(times-2*args.silent):
        os.system("play -nq -t alsa synth .2 sine 1200")


def getData(image, x, y, dx=33, dy=21, **kwargs):
    global tessconfig
    global device
    new = False
    if image is None:
        new = True
        sc = device.screencap()
        screen = io.BytesIO()
        screen.write(sc)
        image = Image.open(screen)
    channel = kwargs.get('channel', 'B')
    negate = kwargs.get('negate', True)
    cropped = image.crop([x, y, x+dx, y+dy]).getchannel(channel)
    if negate:
        cropped = ImageOps.invert(cropped)
    d = pytesseract.image_to_string(
        cropped, lang='eng', config=tessconfig).strip()
    if args.log > 0:
        image.save('./screen.png')
        cropped.save('./cropped.png')
    if new:
        screen.close()
    if args.log > 1:
        input("\nenter to continue...".format(d))
    return d if d.isascii() else ''


def help(device, image):
    try:
        h = int(getData(image, 2157, 687))
        if h > 11:
            beep(5)
            c = input(
                'helps: {}. [Enter] to ignore or [Y] to confirm: '.format(h))
            if c.lower() != 'y':
                h = 0
    except ValueError:
        pass
    else:
        Stats.helps += h
        print("Helped: {}/{}".format(h, Stats.helps))
        beep()
        device.tap(*tapseq['helpr'])


def get_marches(image):
    m, marches = 0, getData(image, 2100, 240, 54, 30).split('/')
    if len(marches) == 2:
        m = int(marches[0])  # there should be no exception here
        Stats.marches = "/".join(marches)
    else:
        confirm(device)
        image.save('./screen.png')
        beep(3)
        conf = input("Enter [Y] to confirm 0 marches, Press"
                     " [Enter] to continue as it is: ")
        if conf.lower() != 'y':
            m = args.marches
        Stats.marches = '{}/{}'.format(m, args.marches)
    return m


def dispatch(tapseqnow):
    success, i = False, 0
    while not success and i < 5:
        device.run(*tapseq['map'], *tapseq['search'], *tapseqnow[0:-2])
        device.tap(*tapseqnow[-2:], sleep=3)
        device.run(*tapseq['gather'][:-4])
        with io.BytesIO() as screen:
            screen.write(device.screencap())
            image = Image.open(screen)
            data1 = getData(image, 1735, 296, 139, 33)
            data2 = getData(image, 1815, 79, 58, 28, channel='G', negate=False)
            if len(data1.split(':')) == 3:
                print("March will take {}".format(data1))
                device.run(*tapseq['gather'][-4:-2])
                success = True
            elif data2 == 'new':
                device.run(*tapseq['gather'][-4:])
                success = True
        device.run(*tapseq['map'])
        i += 1

    if not success:
        print("Failed to dispatch troops after 5 attempts")


def gather_rss():
    global gatherGen
    now = gatherGen.next()
    print("Gathering: {}".format(now))
    tapseqnow = tapseq[now]
    dispatch(tapseqnow)


def kill_barb():
    print("Gathering: barb")
    tapseqnow = tapseq["barb"]
    level = gatherGen.nextbarb()
    if level == 1:
        tapseqnow = tapseqnow[:-2] + tapseq['barbp'] + tapseqnow[-2:]
    elif level == -1:
        tapseqnow = tapseqnow[:-2] + tapseq['barbm'] + tapseqnow[-2:]
    dispatch(tapseqnow)


def gather_barb(ap):
    i, ap = 0, ap-40
    kill_barb()
    if args.chain:
        while ap >= args.ap:
            with io.BytesIO() as screen:
                screen.write(device.screencap())
                image = Image.open(screen)
                confirm(device, image)
                captcha(device, image)
                help(device, image)
                result = getData(
                    image, 1743, 808, 122, 36, negate=False).lower()
                print('{:4d}: {}'.format(i, result), end='\r')
                if result == "victory":
                    ap -= 40
                    kill_barb()
                elif result == "defeat" or get_marches(image) < args.marches:
                    break
                i += 1


def gather(device, image):
    global gatherGen
    m = get_marches(image)
    if m < args.marches-1:
        gather_rss()
    elif m == args.marches-1:
        print("Marches out: {}/{}".format(m, args.marches))
        device.tap(*tapseq['avatar'])
        aplevel = getData(None, 654, 599, 126, 25).split('/')
        print("AP detected: {}".format(aplevel[0]))
        device.run(0, 0)
        ap = 0
        if len(aplevel) == 2:
            try:
                ap = int(aplevel[0].replace(',', ''))
                Stats.ap = ap
            except ValueError:
                pass
        if ap > args.ap:
            gather_barb(ap)
        else:
            gather_rss()


def captcha(device, image, force=False):
    global captcha_i
    captcha_i = (captcha_i+1) % 13
    if force or captcha_i == 0:
        if "Service" in getData(image, 1611, 618, 290, 33, negate=False):
            Stats.captcha_t += [time.strftime("%H:%M", time.gmtime())]
            print(*Stats.captcha_t, sep='\n')
            print('Captcha Detected')
            device.run(*tapseq['captcha'][2:])
            time.sleep(20)
            i = 0
            while i < 20:
                data = getData(None, 1611, 618, 290, 33, negate=False)
                if "Service" not in data:
                    break
                else:
                    i += 1
                    print(" Check: {}".format(i), end='\r')
                    beep(3)
                    time.sleep(3)
            if i == 20:
                print("Captcha not solved in time.")
                raise GameError(True)


def confirm(device, image=None, force=False):
    global confirm_i
    confirm_i = (confirm_i+1) % 13
    if force or confirm_i == 0:
        confirm = getData(image, 1033, 691, 210, 44, channel='G', negate=False)
        if "CONFIRM" == confirm.upper():
            confirm_i = -1
            print("Tapping CONFIRM")
            device.tap(*tapseq['confirm'], sleep=30)
            raise GameError()


def maintenance(device, image):
    Stats.maintenance += 1
    print("Running maintenance")
    device.run(*tapseq['map'], *tapseq['map'])
    if not args.safe:
        device.run(*tapseq['cityrss'])
        for b in ['i', 'ii', 'iii', 'iv']:
            seqnow = tapseq['troopbld_'+b]
            device.run(*seqnow[:2], *seqnow)
            if args.tier is not None:
                device.tap(*tapseq['t'+str(args.tier)])
            with io.BytesIO() as screen:
                screen.write(device.screencap())
                image = Image.open(screen)
                train = getData(
                    image, 1598, 852, 118, 34, channel='G', negate=False)
                if "TRAIN" == train.upper():
                    name = getData(
                        image, 1090, 400, dx=450, dy=44, channel='R')
                    print("Training: {}".format(name))
                    beep()
                    device.tap(*tapseq['train'])
                else:
                    device.run(0, 0)
        device.run(*tapseq['heal'][:2], *tapseq['heal'][:2])
        data = getData(None, 1576, 876, 116, 32, channel='R', Negate=False)
        try:
            time.strptime(data, '%H:%M:%S')
        except ValueError:
            pass
        else:
            print('Healing troops. ETA: {}'.format(data))
            device.run(*tapseq['heal'][-2:], *tapseq['heal'][:-2])
    # run alliance actions every other maintenance
    if Stats.maintenance % 2 == 0:
        device.run(*tapseq['expand'], *tapseq['alliance'][:-2])
        with io.BytesIO() as screen:
            screen.write(device.screencap())
            image = Image.open(screen)
            try:
                techs = int(getData(image, 1357, 762))
            except ValueError:
                techs = 0
            try:
                gifts = int(getData(image, 1555, 762))
            except ValueError:
                gifts = 0
            print("techs:{:3d}, gifts:{:3d}".format(techs, gifts))
            if techs > 5 and args.tech is not None:
                techseq = tapseq['alliance_tech']
                tech_canvas = [380, 268, 1511, 699]
                dx = (2*args.tech[0]-1)*tech_canvas[2]/6
                dy = (2*args.tech[1]-1)*tech_canvas[3]/(2*args.tech[2])
                x = (tech_canvas[0] + dx)*100/device.width
                y = (tech_canvas[1] + dy)*100/device.height
                device.run(*techseq[:2], sleep=6)
                device.run(x, y)
                device.longtap(*techseq[-2:], duration=(techs+1)*1000)
                device.run(0, 0, 0, 0)
            if gifts > 5:
                giftseq = tapseq['alliance_gift']
                device.run(*giftseq[:-2])
                while gifts > 0:
                    gifts -= 1
                    device.tap(*giftseq[-2:])
                device.run(0, 0)
        device.run(*tapseq['alliance'][-2:], *tapseq['expand'])
    else:
        device.run(*tapseq['map'], *tapseq['map'])


def action(device):
    global perf_counter
    global tapseq
    with io.BytesIO() as screen:
        screen.write(device.screencap())
        image = Image.open(screen)
        confirm(device, image, force=True)
        captcha(device, image, force=True)
        help(device, image)
        if getData(image, 1644, 1044, 87, 26) == "Items":
            device.tap(*tapseq['expand'])
        if getData(image, 471, 15, 83, 31).startswith('#'):
            device.tap(*tapseq['map'])
        gather(device, image)
        perf_counter_new = time.perf_counter()
        if perf_counter_new - perf_counter > 60*10:
            perf_counter = perf_counter_new
            maintenance(device, image)


def __pre_exit():
    print('\nexiting...')
    with open('./gatherGen', 'wb') as gg:
        pickle.dump(gatherGen, gg)
    device.event('SLEEP')


def __gcd(x, y):
    return x if y == 0 else __gcd(y, x % y)


parser = argparser()
argv = sys.argv
args = parser.parse_args(argv[1:])
client = Client(host=args.host, port=args.port)
device = Device(getAdbDevice(client))
tessconfig = "--oem 3 --psm 7 --dpi {}".format(device.dpi)
# -c tessedit_char_whitelist=0123456789:/# CONFIRMtemsK


if __name__ == "__main__":
    confirm_i, captcha_i = -1, -1
    perf_counter = time.perf_counter()
    pid = os.getpid()
    gcd = __gcd(device.width, device.height)
    aspectratio = "{}x{}".format(device.width//gcd, device.height//gcd)
    tapseqf = "./{}.tapseq".format(aspectratio)
    with open(tapseqf, 'r') as f:
        tapseq = json.load(f)
    try:
        with open('./gatherGen', 'rb') as gg:
            gatherGen = pickle.load(gg)
    except FileNotFoundError:
        gatherGen = GatherGen()

    br = int(device.shell('settings get system screen_brightness'))
    if br > 0:
        print("Trying to decrease the brightness")
        for i in range(1+br//26):
            device.event('BRIGHTNESS_DOWN')
        device.event('BACK')

    am = 'am start -n com.lilithgame.roc.gp/com.harry.engine.MainActivity'
    if "Warning:" not in device.shell(am):
        print('Trying to start Rise of Kingdoms')
        time.sleep(30)

    i = 0
    while True:
        print("\033c\033[3J", end='')
        print("{}, UTC: {}, pid: {}".format(
            device, time.strftime("%H:%M", time.gmtime()), pid))
        print("{:<7d} {}".format(i, Stats.summary()))
        try:
            action(device)
        except GameError as e:
            if e.quit:
                __pre_exit()
                break
        except KeyboardInterrupt:
            i -= 1
            cmd = ' '.join(argv)
            print('',
                  'Modify the command to continue or press [Enter] to exit:',
                  sep='\n')
            readline.set_startup_hook(lambda: readline.insert_text(cmd))
            new = input().strip()
            readline.set_startup_hook(None)
            if not new:
                pass
            elif new == cmd:
                __pre_exit()
                break
            else:
                argv = new.split()
                args = parser.parse_args(argv[1:])
        except Exception:
            traceback.print_exc(1)
            beep(5)
            input('Enter to continue...')
        i += 1
    beep(2)
