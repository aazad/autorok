# autorok

Keep Rise of Kingdoms running while afk

![sample](.media/sample.png)

## Requirements
`pip install pure-python-adb pillow pytesseract`  
`pacman -Syu tesseract sox android-tools`  

## Capabilities

- [x] Respond to help req
- [x] Gather rss
- [x] Kill barbs
  - [x] Barb chain (experimental)
- [x] Alliance rss collect
- [x] Alliance gift collect
- [x] Alliance tech donate
- [ ] Autorally
- [ ] Build flags
- [ ] VIP collect
- [ ] Lyceum
- Layout specific 
  - [x] City rss collect
  - [x] Train troops
  - [x] Heal troops
  - [ ] Explore map
  - [ ] Tavern collect
  - [ ] Blacksmith

Solving captcha is not in the scope of this script, and will never be. When a captcha is detected, the script will pause until you solve the captcha yourself. If the captcha is not solved within 20 checks, script will stop.

## Usage

Currently targeted for Linux. Connect the physical device with USB cable (or over tcpip) and start ADB server.  

Script will reduce the brightness to minimum, and start RoK.  

`^Z` Suspend script (serves as pausing the script)  
`fg` Resume script  
`^C` Keyboard interrupt will give you a chance to modify the command arguments and continue with the updated args. Clear the input field to continue as it is. If the input field is not modified at all, script will exit. Device is locked while exiting  

### Arguments

`    -h` Show the help message  


`    -H` ADB Server hostname or IP. Default: 127.0.0.1  
`    -p` ADB Server port. Default: 5037  
`    -d` Index of the device in ADB devices, starting from 0. Default: 0  

`--safe` Skip city-layout specific actions eg. training troops  
`    -c` Chain barbarians, ie kill one after another before commander returns to the city  
`    -m` Maximum marches to use. Default: 4  
`    -a` Minimum action points to save. Default: 840. Useful if you want to do forts anytime  
`    -t` Alliance tech research location. Format: \[Column\]\[Row\]\[Total rows\]. eg: if the 1st column has 4 rows, 124 will donate to the 1st column, 2nd row  
`    -T` Troop tier to select while training troops. Skip to use auto-select in the building  

`    -l` Log level 1: Save the screenshot and the cropped image received by tesseract  
`   -ll` Log level 2: Also show data each time tesseract is used and wait to continue  
`    -s` Silent level 1: No beep while exiting, and helping; one beep for debug, and captcha; two beeps for error  
`   -ss` Silent level 2: One beep for error. No beep otherwise  
`  -sss` Silent level 3: Total silence  

Any other arguments will make the script abort.

exampe: `./rok.py -sm5 -T3 -t 124`
